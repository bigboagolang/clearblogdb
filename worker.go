package main

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/joho/godotenv"
	"gopkg.in/resty.v1"
	"log"
	"os"
	"strconv"
	"time"
)

type Worker struct {
	Days             int
	BlogUrl          string
	AuthUrl          string
	AuthClientId     string
	AuthClientSecret string
	User             string
	Password         string
}

func NewWorker() *Worker {
	return &Worker{}
}

func (w *Worker) Run() {
	err := godotenv.Load()
	if err != nil {
		log.Fatalf("Error getting env, %v", err)
	} else {
		log.Println("We are getting the env values")
	}
	w.AuthUrl = os.Getenv("AUTH_URL")
	w.AuthClientId = os.Getenv("AUTH_CLIENT_ID")
	w.AuthClientSecret = os.Getenv("AUTH_CLIENT_SECRET")
	w.BlogUrl = os.Getenv("BLOG_URL")
	d, err := strconv.Atoi(os.Getenv("DAYS"))
	if err != nil {
		log.Fatal("unable to parse DAYS parameter from env file")
	}
	w.Days = d
	w.User = os.Getenv("USER")
	w.Password = os.Getenv("PASSWORD")

	w.do()
}

func (w *Worker) do() {
	d := time.Hour * 24 * time.Duration(w.Days)
	type DeletedAttach struct {
		Id          uuid.UUID `json:"id"`
		Deleted     bool      `json:"deleted"`
		DeletedBy   uuid.UUID `json:"deleted_by"`
		DeletedWhen time.Time `json:"deleted_when"`
	}

	c := resty.New()
	//get access token
	t, err := GetAccessToken(c, w)
	if err != nil {
		log.Fatalln("unable to get auth token: ", err.Error())
	}
	//get object marker to delete
	resp, err := c.R().
		SetHeader("Authorization", "Bearer "+t.Token).
		Get(w.BlogUrl)

	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(string(resp.Body()))
	if len(resp.Body()) == 0 {
		return
	}
	res := make([]DeletedAttach, 0)
	err = json.Unmarshal(resp.Body(), &res)
	if err != nil {
		log.Fatalln(err)
	}
	//check delete criteria and delete if success
	for _, v := range res {
		//Since returns the time elapsed since t
		if time.Since(v.DeletedWhen) >= d {
			resp, err = c.R().
				SetHeader("Authorization", "Bearer "+t.Token).
				SetPathParams(map[string]string{"id": v.Id.String()}).
				Delete(w.BlogUrl + "/{id}")
			if err != nil {
				log.Println(err)
			}
		}
	}
}
