module gitlab.com\bigboagolang\clearblogdb

go 1.12

require (
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	gopkg.in/resty.v1 v1.12.0
)
