package main

import (
	"encoding/json"
	"gopkg.in/resty.v1"
	"log"
)

type AccessToken struct {
	Token            string `json:"access_token"`
	ExpiresIn        int    `json:"expires_in"`
	RefreshExpiresIn int    `json:"refresh_expires_in"`
	RefreshToken     string `json:"refresh_token"`
	TokenType        string `json:"token_type"`
	NotBeforePolicy  int    `json:"not-before-policy"`
	SessionState     string `json:"session_state"`
	Scope            string `json:"scope"`
}

func GetAccessToken(c *resty.Client, w *Worker) (*AccessToken, error) {
	r := &AccessToken{}
	resp, err := c.R().
		SetBody(`username=`+w.User+`&password=`+w.Password+`&client_id=`+w.AuthClientId+`&grant_type=password&client_secret=`+w.AuthClientSecret).
		SetHeader("Content-Type", "application/x-www-form-urlencoded").
		Post(w.AuthUrl)

	if err != nil {
		log.Printf("Error while gettign the token: %v", err)
		return nil, err
	}

	err = json.Unmarshal(resp.Body(), r)
	if err != nil {
		log.Printf("Can't deserislize: %v\n", resp.Body())
		return nil, err
	}
	return r, nil
}
